#phpUsageScanner
##find the most common PHP functions used
##graph the data

---

####Limit the scope of the PHP language learning curve

Create a must learn PHP function list. As well as other language constructs.

####Usage:

Run:

```bash
composer update
```

Generate Data:

```bash
cd data/
php php_function_list_generator.php
```

```bash
cd test/general_packages
composer update
```

***CLI:***

--help: For help

```bash
php run.php --help
```


-dir: Directory

--output: Output log file

--echo: Echo output to shell (Default: true)

--ignore: Ignore directory (Comma separated)

```bash
php run.php --dir ./test/general_packages/ --output="./output/php_test.log"
```

####Summary:

Just like common languages programming languages as PHP should adhere to the Pareto principle ***(80 - 20 rule)*** 

https://en.wikipedia.org/wiki/Pareto_principle

https://www.fluentin3months.com/80-20-rule/

>This rule is applied in so many fields and in its simplest form, basically states that you get 80% of the results from 20% of the work. (Other interpretations are possible, like 80% of the wealth of a country typically belonging to 20% of its population).
>
>While there is some merit to using the number 80, the actual quantity doesn't interest me as much as the fact that you get most of the end-results from a fraction of what you put into something.


####Theory:

Since PHP has around 10,000 functions ( http://php.net/manual/en/indexes.functions.php ), according to the Pareto principle roughly 2,000 of these core PHP functions should be used in about 80% of the cases within common PHP libraries.

####Hypothesis:

Since PHP is a niche programming language where the use cases are limited there should only be a few groups of common functions. Most likely the Pareto principle is wrong when it comes to PHP. In theory most projects will only need to accomplish a few things ie; connect and manipulate database data, manipulate file and folders, read and generate strings, as well as generating and validating 2D images, also allot of array / list manipulation with conditional logic.

I have not taken the time to read the entire list of the available PHP functions.

I would expect at least 50% of the PHP array functions to be in the 20% of the principle ( http://php.net/manual/en/function.array.php ). Same with ( http://php.net/manual/en/function.gd-info.php ) and ( http://php.net/manual/en/class.gmagick.php ) for image processing concurrently. 

I could only really think of a few hundred functions that are used daily in PHP. To be honest this is the same with the spoken English language. Since the common spoken English language is primarily used for day to day communication I would assume that only a few people in key industries would utilize certain groups of words. Example would be these professions; Chemist, Physicist and Engineers are more likely to use scientific terms for certain chemicals or elements that are on the periodic table, rather than common folk.

####Test Data:

**./test/general_packages**

Include the most common PHP Web Frameworks

Include the most common PHP based templating languages

Include the most common 2D image processing libraries

Include ORMs

Include some random packages big and small

####Conclusion:

?
