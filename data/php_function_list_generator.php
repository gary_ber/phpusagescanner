<?php

/**
 * http://php.net/manual/en/indexes.functions.php
 * Roughly 10k functions
 **/

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$loader = require '../vendor/autoload.php';

use \Wa72\HtmlPageDom\HtmlPage;

$html = file_get_contents('http://php.net/manual/en/indexes.functions.php');

$doc = new HtmlPage($html);

$list = $doc->filter('.gen-index.index-for-refentry')->filter('a.index');

$phpFunctions = [];

/**
 * @var $item DOMElement
 */
foreach($list as $item)
{
    $functionName = $item->nodeValue;
    $functionDescription = 'Test';
    $functionURI = $item->getAttribute('href');
    array_push($phpFunctions, [
        $functionName,
        $functionDescription,
        $functionURI
    ]);
}

$phpFunctionListFile = 'php_function_list.csv';

/*
 * Empty the file first
 */

$f = @fopen($phpFunctionListFile, "r+");
if ($f !== false) {
    ftruncate($f, 0);
    fclose($f);
}

/*
 * Write data to file
 */

$fp = fopen($phpFunctionListFile, 'w');

foreach ($phpFunctions as $phpFunction) {
    fputcsv($fp, $phpFunction);
}

fclose($fp);

