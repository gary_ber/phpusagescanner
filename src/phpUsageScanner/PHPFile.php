<?php

namespace phpUsageScanner;

/**
 * Class PHPFile
 * @package phpUsageScanner
 */

class PHPFile
{

    protected $fileLocation = '';

    /**
     * @var Usage $usage
     */
    protected $usage;

    /**
     * @var Log $log
     */
    protected $log;

    /**
     * @var array $foundFunctions
     */
    protected $foundFunctions = [];

    /**
     * PHPFile constructor.
     * @param String $fileLocation
     * @param Usage $usage
     * @param Log $log
     * @throws \Exception
     */
    public function __construct(String $fileLocation, Usage &$usage, Log &$log)
    {
        if (!is_file($fileLocation)){
            throw new \Exception('PHP File not found.');
        }
        $this->fileLocation = $fileLocation;
        $this->usage = $usage;
        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function runCheck()
    {
        $PHPFileContents = file_get_contents($this->getFileLocation());
        if ($PHPFileContents) {
            $lineNumber = 0;
            $tokensArray = token_get_all($PHPFileContents);
            $functionDefining = false;
            $lastStringToken = null;
            foreach($tokensArray as $token){
                if(is_array($token)){
                    $tokenNamed = array(
                        'name' => token_name($token[0]),
                        'value' => $token[1],
                        'line' => $token[2]
                    );
                    $lineNumber = $tokenNamed['line'];
                }else{
                    $tokenNamed = array(
                        'name' => 'T_NA',
                        'value' => $token,
                        'line' => $lineNumber
                    );
                }
                // TODO check for Static class methods
                switch($tokenNamed['name'])
                {
                    case 'T_FUNCTION':
                        $functionDefining = true;
                        break;
                    case 'T_STRING':
                        if($functionDefining === false){
                            $lastStringToken = $tokenNamed;
                        }
                        break;
                    case 'T_NA':
                        switch($tokenNamed['value'])
                        {
                            case '(':
                                $functionDefining = false;
                                if(is_array($lastStringToken))
                                {
                                    $foundFunction = $this->usage->findFunction($lastStringToken['value']);
                                    if($foundFunction !== false)
                                    {
                                        $this->log->foundPHPFunction($this, $lastStringToken['value'], $lastStringToken['line']);
                                        array_push($this->foundFunctions, $foundFunction);
                                    }
                                    $lastStringToken = null;
                                }
                                break;
                            case ')':
                                break;
                        }
                        break;
                    case 'T_WHITESPACE':
                        break;
                    default:
                        $functionDefining = false;
                        break;
                }
            }
            return $this->foundFunctions;
        }
        $this->log->emptyPHPFile($this);
        return false;
    }

    /**
     * @return string
     */
    public function getFileLocation()
    {
        return $this->fileLocation;
    }

}