<?php

namespace phpUsageScanner;

/**
 * Class Usage
 * @package phpUsageScanner
 */

class Usage {

    /**
     * @var array $functionList
     */
    protected $functionList = [];

    /**
     * Usage constructor.
     * @param String $phpFunctionsListFile
     * @throws \Exception
     */
    public function __construct(String $phpFunctionsListFile)
    {
        if(!file_exists($phpFunctionsListFile))
        {
            throw new \Exception('phpFunctionsListFile not found.');
        }
        $this->functionList = $this->generateFunctionList($phpFunctionsListFile);
    }

    /**
     * @param String $phpFunctionsListFile
     * @return array
     */
    private function generateFunctionList(String $phpFunctionsListFile)
    {
        $functionList = [];
        $file = fopen($phpFunctionsListFile, 'r');
        while (($line = fgetcsv($file)) !== FALSE) {
            $function = new \stdClass();
            $function->name = $line[0];
            $function->description = $line[1];
            $function->uri = $line[2];
            $functionList[$line[0]] = $function;
        }
        fclose($file);
        return $functionList;
    }

    /**
     * @return array
     */
    public function getFunctionList()
    {
        return $this->functionList;
    }

    /**
     * @param String $functionName
     * @return mixed
     */
    public function findFunction(String $functionName)
    {
        if(isset($this->functionList[$functionName]))
        {
            return $this->functionList[$functionName];
        }
        return false;
    }

}
