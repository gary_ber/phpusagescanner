<?php

namespace phpUsageScanner;

/**
 * Class Scanner
 * @package phpUsageScanner
 */

class Scanner {

    /**
     * @var Usage $usage
     */
    protected $usage;

    /**
     * @var Log $log
     */
    protected $log;

    /**
     * Scanner constructor.
     * @param String $phpFunctionsListFile
     * @param String $outputLogFile
     * @param bool $echoOutput
     * @throws \Exception
     */
    public function __construct(String $phpFunctionsListFile, String $outputLogFile = '', Bool $echoOutput = false)
    {
        $this->log = new Log($outputLogFile, $echoOutput);
        $this->usage = new Usage($phpFunctionsListFile);
    }

    /**
     * Find and check PHP files
     *
     * @param String $directory
     * @param array $ignoreList
     * @return Log
     */
    public function scan(String $directory, Array $ignoreList=[])
    {
        $this->findPHPFiles($directory, $ignoreList);
        return $this->log;
    }

    /**
     * @param String $directory
     * @param array $ignoreList
     */
    private function findPHPFiles(String $directory, Array $ignoreList=[])
    {
        if(is_dir($directory))
        {
            $this->scanDir($directory, $ignoreList);
        }
        elseif (is_file($directory))
        {
            try {
                $PHPFile = new PHPFile($directory, $this->usage,$this->log);
                $this->addPHPFile($PHPFile);
            }
            catch (\Exception $e) {
                $this->log->invalidPHPFile($PHPFile);
            }
        }
    }

    /**
     * Recursive function
     *
     * @param String $target
     * @param array $ignoreList
     */
    private function scanDir(String $target, Array $ignoreList=[]) {
        foreach($ignoreList as $ignored)
        {
            if(realpath($target) === realpath($ignored))
            {
                return;
            }
            else if(is_dir(realpath($target)) && (strpos(realpath($target), dirname(realpath($ignored))) === 0))
            {
                return;
            }
        }
        if(is_dir($target)){
            $files = glob($target . '*', GLOB_MARK);
            foreach($files as $file)
            {
                $this->scanDir($file, $ignoreList);
            }
        }
        elseif (is_file($target))
        {
            $extension = strtolower(pathinfo($target, PATHINFO_EXTENSION));
            if($extension == 'php')
            {
                try {
                    $PHPFile = new PHPFile($target,$this->usage,$this->log);
                    $this->addPHPFile($PHPFile);
                }
                catch (\Exception $e) {
                    $this->log->invalidPHPFile($PHPFile);
                }
            }
        }
    }

    /**
     * @param PHPFile $PHPFile
     */
    private function addPHPFile(PHPFile $PHPFile)
    {
        $this->log->foundPHPFile($PHPFile);
        if($PHPFile->runCheck() !== false)
        {

        }
    }

}
